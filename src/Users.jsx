import React, {useEffect, useState} from "react";

export const Users = () => {
    const [users, setUsers] = useState([]);
    const [page, setPage] = useState(1);
    const [loading, setLoading] = useState(true);
    const [limit, setLimit] = useState(9);

    window.onscroll = function (ev) {
        handleScroll();
    };

    const handleScroll = () => {
        if (((window.innerHeight + window.scrollY) + 1) >= document.body.offsetHeight) {
            if (!loading) {
                setPage(prev => prev + 1);
            }
        }
    }

    useEffect(() => {
        getData();
    }, [page]);

    const getData = async () => {
        setLoading(true);
        const data = await fetch(`https://jsonplaceholder.typicode.com/comments?_limit=${limit}&_page=${page}`);
        const res = await data.json();
        setUsers(prev => [
            ...prev,
            ...res
        ]);
        setLimit(3);
        setLoading(false);

    }

    return (
        <>
            <div className="container">
                <div className="row">
                    {
                        users?.map((item, i) => (
                            <div key={i} className="col-sm-12 col-md-4 border border-3 mt-2 mb-2">
                                <div className="row">
                                    <div className="col-md-6">
                                        <img width="100" height="100"
                                             src={`https://avatars.dicebear.com/api/micah/${item.id}.svg`}
                                             alt="img"/>
                                        <p><b>Name</b> : {item.name}</p>
                                        <p><b>Email</b>: {item.email}</p>
                                    </div>
                                    <div className="col-md-6">
                                        <p><b>Description</b> : {item.body} </p>
                                    </div>
                                </div>
                            </div>
                        ))
                    }
                </div>
                {loading && <div className="d-flex justify-content-center">
                    <div className="spinner-border" role="status"></div>
                </div>}
            </div>
        </>
    )
}