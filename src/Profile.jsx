import React, {useState} from 'react';
import $ from "jquery";

export default function Profile() {
    const [view, setView] = useState("x");
    const [user, setUser] = useState([]);
    const [users, setUsers] = useState([]);
    const valueChange = (e) => {
        const {name, value} = e.target;
        setUser(prev => ({
            ...prev,
            [name]: value
        }))
    }

    const add = () => {
        setUsers(prev => [user, ...prev]);
        setUser([]);
        $('.slider').removeClass("next-form");
    }

    return (
        <>
            <div className="container">
                <div className="text-center my-5 packet">
                    <div className="slider">
                        <div className="left">
                            <div className="box">
                                <h2>Form</h2>
                                <div className="mb-2">
                                    <input className="form-control" onChange={(e) => valueChange(e)}
                                           value={user.name || ""} name="name"
                                           placeholder="Name"/>
                                </div>
                                <div className="mb-2">
                                    <input className="form-control" onChange={(e) => valueChange(e)}
                                           value={user.email || ""}
                                           name="email"
                                           placeholder="Email"/>
                                </div>
                                <button type="submit" className="btn btn-primary next">
                                    Next
                                </button>
                            </div>
                        </div>
                        <div className="right">
                            <div className="box right-box">
                                <h2>Form</h2>
                                <div className="mb-2">
                                    <textarea rows={4} onChange={(e) => valueChange(e)} className="form-control"
                                              name="description"
                                              value={user.description || ""}
                                              placeholder="Description"/>
                                </div>
                                <button className="btn btn-secondary mr-5 back float-start">
                                    prev
                                </button>
                                <button className="btn btn-primary add float-end" onClick={() => add()}>
                                    Add
                                </button>
                            </div>
                        </div>
                    </div>
                </div>

                <h3 className="mb-5">Profiles</h3>

                <div style={{height: "500px", overflowX: "hidden", overflowY:"auto"}}>
                    <div className="row d-flex">
                        {
                            users?.map((item, i) => (
                                <div key={i} className="col-sm-12 col-md-4 px-3 mt-2 mb-2">
                                    <div className="border border-3">
                                        <div className="row p-2">
                                            <div className="col-md-4">
                                                <img width="100" height="100"
                                                     src={`https://avatars.dicebear.com/api/micah/${item.name}.svg`}
                                                     alt="img" className="img-fluid"/>
                                                <p><b>Description :</b> {item.description} </p>
                                            </div>
                                            <div className="col-md-8">
                                                <p><b>Name :</b> {item.name}</p>
                                                <p><b>Email :</b> {item.email}</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            ))
                        }
                    </div>
                </div>
            </div>
        </>
    )
}

$(document).on('click', '.next', function () {
    $('.slider').addClass('next-form');
});

$(document).on('click', '.back', function () {
    $('.slider').removeClass('next-form');
});